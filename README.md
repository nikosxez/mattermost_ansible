# Mattermost installation with ansible

## Target system

1. rhel 8 based system
2. ssh access from ansible host


## Ansible host requirements

1. Ansible 2.9 or greater
2. Install mysql and posix roles from Ansible Galaxy
```
ansible-galaxy collection install community.mysql
ansible-galaxy collection install ansible.posix
```

## Ansible commands

Εxecute the following commands in sequence
```
ansible-playbook system-play.yml -i inventory.txt
ansible-playbook mysql-play.yml -i inventory.txt
ansible-playbook mattermost-play.yml -i inventory.txt

For password authentication install sshpass and use the argument -kK 
```

